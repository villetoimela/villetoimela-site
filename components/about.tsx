import Image from "next/image"
import Link from "next/link"
// @ts-ignore
import Fade from "react-reveal/Fade"

/* eslint-disable-next-line */
export interface AboutProps {
  imgSrc: string
}

export function About({ imgSrc }: AboutProps) {
  return (
    <div id="about" className="mx-auto w-full overflow-hidden px-11 py-44 ">
      <div className="relative mx-auto max-w-7xl xl:px-0">
        <p className="pb-4 text-xs font-normal italic tracking-widest text-gray-400">
          {" "}
          {"< about >"}{" "}
        </p>
        <Fade right>
          <h2 className="font-Syne text-3xl font-extrabold tracking-widest text-white sm:text-7xl">
            MINUSTA
          </h2>
        </Fade>
        <div className="mt-4 border border-red-300 sm:mb-20"></div>
        <div className="flex flex-col items-center lg:flex-row">
          <div className="mt-16 flex w-full animate-float items-center pb-10 lg:mt-0 lg:w-1/3 lg:pb-0 lg:pr-24">
            <Fade left>
              <Image src={imgSrc} width={2340} height={1761} />
            </Fade>
          </div>
          <div className="relative w-full lg:w-2/3">
            <div className=" overflow-hidden">
              
                <p className="font-Syne text-base font-bold tracking-wider text-white ">
                  Tervehdys!
                  <Fade bottom>
                  <p className="pb-4 pt-4">
                    {" "}
                    Viime vuosina olen tosissani ryhtynyt toteuttamaan unelmaani devaajana, ja olenkin onnistunut saavuttamaan ensimmäiset tavoitteeni tällä tiellä.
                  </p>{" "}
                  </Fade>
                  <Fade bottom>
                  <p className="pb-4">
                    {" "}
                    Olen erittäin motivoitunut ohjelmistokehittäjän alku, jolla on palava halu kehittyä ja saavuttaa huippuosaaminen alallani. 
                    Oma yritykseni on mahdollistanut minulle hieman kokemusta monipuolisista projekteista, joissa olen työskennellyt joko yksin tai osana pientä tiimiä. 
                    Nykyään olen myös etuoikeutetussa asemassa, sillä toimin web-kehittäjänä yhdessä Suomen merkittävimmistä markkinointitoimistoista. 
                    Tässä roolissa olen päässyt toteuttamaan asiakkaille monimutkaisiakin kokonaisuuksia, mikä on entisestään vahvistanut osaamistani ja 
                    intoani kehittää teknisiä ratkaisuja, jotka vastaavat asiakkaiden tarpeisiin ja odotuksiin.{" "}
                  </p>{" "}
                  </Fade>
                  <Fade bottom>
                  <p className="pb-4">
                    Vuonna 2022 alussa perustimmekin kahden miehen voimin
                    yrityksen. Käy kahlaamassa{" "}
                    <Link href={"https://www.hiisi.digital/"}>
                      <a className="text-red-300">Hiisi Digital</a>
                    </Link>
                    in sivut jos asia kiinostelee!
                  </p>{" "}
                  </Fade>
                  <Fade bottom>
                  <p>
                    {" "}
                    Olen tykittäny kymmeniä tuhansia tunteja hc-hikoilua pelien
                    parissa. Seuraava tavoite on saada vähintäänkin vastaava
                    lukema mittariin, mutta ohjelmistokehityksen parissa. Ja
                    nälkä vain kasvaa tehdessä!
                  </p>
                  </Fade>
                </p>
              
              <Fade right>
                <p className="mt-12 font-Syne text-sm font-bold uppercase italic text-red-300">
                  " Software developer or a professional Google searcher? "
                </p>
              </Fade>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default About
