import Image from "next/image"
import { SocialIconItem } from "./social-icon-item"
// @ts-ignore
import Fade from "react-reveal/Fade"

/* eslint-disable-next-line */
export interface AboutReverseProps {
  imgSrc: string
}

export function AboutReverse({ imgSrc }: AboutReverseProps) {
  return (
    <div className="mx-auto w-full px-11 py-44 ">
      <div className="relative mx-auto max-w-7xl xl:px-0">
        <Fade left>
          <h2 className="font-Syne text-3xl font-extrabold tracking-widest text-white sm:text-7xl">
            LISÄÄ MINUSTA
          </h2>
        </Fade>
        <div className="mt-4 border border-red-300 sm:mb-20"></div>
        <div className="flex flex-col items-center pt-10 lg:flex-row">
          <div className="relative w-full lg:w-2/3">
            
              <div>
                <p className="font-Syne text-base font-bold tracking-wider text-white ">
                <Fade bottom>
                  <p className="pb-4">
                    {" "}
                    Olen jo hieman kokemusta omaava ohjelmistokehittäjä, jolla on laaja-alainen osaaminen eri teknologioista. 
                    Jo ennen opintojani olen omatoimisesti opiskellut ja harrastellut ohjelmointia, keskittyen aluksi lähinnä front-end -puoleen. 
                    
                  </p>{" "}
                  <Fade bottom>
                  <p className="pb-4">
                    Opintojeni myötä ja jälkeen olen kuitenkin laajentanut osaamistani ja saanut uusia näkökulmia myös backend-kehitykseen.
                    Olen ehtinyt toteuttamaan webbisovelluksia käyttäen esimerkiksi Reactia, Next.js:ää, TailwindCSS:ää ja JavaScript/TypeScript -teknologioita. 
                    Lisäksi minulla on nykyään tuoretta kokemusta PHP:stä ja Wordpress-kehityksestä, ja olenkin jo ehtinyt kerryttää tietoa ja taitoa näistä aiheista ammattilaisena.{" "}
                  </p>{" "}
                  </Fade>
                  </Fade>
                  <Fade bottom>
                  <p className="pb-4">
                    Kaiken kaikkiaan tahdon olla hyvin monipuolinen ohjelmistokehittäjä ja minulta löytyykin vahva intohimo uusien teknologioiden omaksumiseen!
                  </p>{" "}
                  </Fade>
                </p>
              </div>
            
            <Fade bottom>
              <div className="z-50 flex items-center justify-around gap-4 pt-10 lg:justify-start">
                <SocialIconItem
                  imgSrc={"/icon-instagram.svg"}
                  width={42}
                  height={42}
                  linkUrl={"https://www.instagram.com/villetoimela/"}
                />
                <SocialIconItem
                  imgSrc={"/icon-gitlab.svg"}
                  width={50}
                  height={50}
                  linkUrl={"https://gitlab.com/villetoimela"}
                />
                <SocialIconItem
                  imgSrc={"/icon-linkedin.svg"}
                  width={43}
                  height={43}
                  linkUrl={
                    "https://www.linkedin.com/in/ville-toimela-7ba2841b5/"
                  }
                />
                <SocialIconItem
                  imgSrc={"/icon-whatsapp.svg"}
                  width={45}
                  height={45}
                  linkUrl={"https://wa.me/358405137883"}
                />
              </div>
            </Fade>
            <div className="mt-4 border border-white sm:mb-20"></div>
          </div>
          <div className="mt-16 flex w-full animate-float items-center overflow-hidden lg:mt-0 lg:w-1/3 lg:pb-0 lg:pr-24">
            <Fade right>
              <Image src={imgSrc} width={1122} height={1912} />
            </Fade>
          </div>
        </div>
        <p className="text-xs font-normal italic tracking-widest text-gray-400">
          {" "}
          {"< /about >"}{" "}
        </p>
      </div>
    </div>
  )
}

export default AboutReverse
